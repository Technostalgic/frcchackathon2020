﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
	[SerializeField]
	float hp = 10;
	[SerializeField]
	float maxHp = 10;
	[SerializeField]
	float power = 10;

	public float currentHealth => hp;
	public float maxHealth => maxHp;
	public float attack => power;

	public bool isAlive => hp > 0;

	public void initializePlayer()
	{
		hp = 100;
		power = 5;
	}
	public void initializeSkeleton()
	{
		hp = 10;
		power = 5;
	}
	public void initializeZombie()
	{
		hp = 20;
		power = 10;
	}
	public void initializeMummy()
	{
		hp = 30;
		power = 20;
	}

	public void heal(float hpRestore)
	{
		hp = Mathf.Min(hp + hpRestore, maxHp);
	}

	public void takeDamage(float damage)
	{
		Character c = GetComponent<Character>();

		hp -= damage;

		if (c != null)
		{
			c.Flash(Color.red);

			if(!isAlive)
				c.Die();
		}
	}
}