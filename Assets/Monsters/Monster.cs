﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Character
{
	private void OnCollisionStay2D(Collision2D collision) {
		float damage = 10.0f;

		Player playerCollide = collision.collider.GetComponent<Player>();
		if(playerCollide == null)
			playerCollide = collision.otherCollider.GetComponent<Player>();

		if(playerCollide != null) {
			playerCollide.stats.takeDamage(damage);
		}
		else {; }
	}
}