﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MovementBase
{
   
    private Rigidbody2D monster;
    Vector3 initialPosition;
    int direction;
    float maxDist;
    float minDist;
    public float movingSpeed = 5f;

    void Start()
    {
        monster = GetComponent<Rigidbody2D>();
        initialPosition = transform.position;
        direction = -1;
        maxDist += transform.position.x;
        minDist -= transform.position.x;
        monster.velocity = new Vector2(-movingSpeed, monster.velocity.y);
    }
    // Update is called once per frame
    public override void HandleMovement()
    {
        switch (direction)
        {
            case -1:
                // Moving Left
                if (transform.position.x > minDist)
                {
                    monster.velocity = new Vector2(-movingSpeed, monster.velocity.y);
                }
                else
                {
                    direction = 1;
                }
                break;
            case 1:
                //Moving Right
                if (transform.position.x < maxDist)
                {
                    monster.velocity = new Vector2(movingSpeed, monster.velocity.y);
                }
                else
                {
                    direction = -1;
                }
                break;
        }
    }  
}
