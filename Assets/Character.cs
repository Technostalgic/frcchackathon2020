﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	public Stats stats = default;
	public MovementBase movement;
	private float flashTime = 0;
	
	public void Flash(Color col, float length = 0.1f) {
		GetComponent<SpriteRenderer>().color = col;
		flashTime = 0.1f;
	}

	public virtual void Die() {
		Destroy(gameObject);
	}

	public virtual void Update() {
		movement.HandleMovement();

		if(flashTime > 0) {
			flashTime -= Time.deltaTime;
		}
		else if(flashTime < 0) {
			GetComponent<SpriteRenderer>().color = Color.white;
			flashTime = 0;
		}
	}
}
