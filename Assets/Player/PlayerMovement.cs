﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PlayerMovement : MovementBase
{
	public float speed = 200f;
	public float jumpSpeed = 100f;
	public float jumpSustain = 0.45f;
	private float lastMov;

	private Rigidbody2D rBody;
	public float groundCheckRadius;
	private bool isTouchingGround;

	private List<Collider2D> ignoredPlatforms = new List<Collider2D>();

	void Start()
	{
		rBody = GetComponent<Rigidbody2D>();
	}

	public void handleGroundCheck() {

		Collider2D col = Physics2D.OverlapPoint((Vector2)transform.position + Vector2.down * groundCheckRadius);
		isTouchingGround = col != null;
	}

	public override void HandleMovement() {

		if(!Input.GetButton("Jump"))
			handleGroundCheck();

		float movement = Input.GetAxis("Horizontal");

		if(movement > 0) {
			rBody.velocity = new Vector2(movement * speed, rBody.velocity.y);
		}
		else if(movement < 0) {
			rBody.velocity = new Vector2(movement * speed, rBody.velocity.y);
		}
		else {
			rBody.velocity = new Vector2(0, rBody.velocity.y);
			if(lastMov != 0 && isTouchingGround) {
				rBody.velocity = new Vector2(rBody.velocity.x, 0);
			}
		}

		lastMov = movement;

		if(Input.GetButtonDown("Jump") && isTouchingGround)
		{
			isTouchingGround = false;
			rBody.velocity = new Vector2(rBody.velocity.x, jumpSpeed);
		}
		else if(Input.GetButton("Jump") && rBody.velocity.y > 0) {
			rBody.velocity += Physics2D.gravity * -jumpSustain * Time.deltaTime;
		}

		if(Input.GetAxis("Vertical") < 0) {

			RaycastHit2D hit = Physics2D.Raycast(rBody.position, Vector2.down, 5, LayerMask.GetMask("Platforms"));
			if(hit.collider == null)
				return;

			if(hit.collider.GetComponent<PlatformEffector2D>() != null) {
				ignoredPlatforms.Add(hit.collider);
				Physics2D.IgnoreCollision(GetComponent<Collider2D>(), hit.collider, true);
			}
		}
		else if(ignoredPlatforms.Count > 0) {

			Collider2D self = GetComponent<Collider2D>();
			foreach(Collider2D col in ignoredPlatforms) {
				Physics2D.IgnoreCollision(self, col, false);
			}

			ignoredPlatforms.Clear();
		}
	}
}
