﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
	[SerializeField]
	private Camera cam;
	private float attackWait = 0;

	[SerializeField]
	private float attackCooldown = 0.35f;
	public Projectile attackProjectile = default;

	public override void Update() {
		base.Update();

		if(attackWait > 0)
			attackWait -= Time.deltaTime;

		if(Input.GetMouseButton(0)) {
			if(attackWait <= 0) {
				attack();
			}
		}
	}

	public override void Die() {
		cam.transform.parent = null;
		base.Die();
	}

	private void attack() {
		attackWait = attackCooldown;

		Vector2 mouse = Input.mousePosition;
		mouse = cam.ScreenToWorldPoint(mouse);

		float direction = Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x);

		Projectile.Fire(attackProjectile, transform.position, direction, 25);
	}
}
