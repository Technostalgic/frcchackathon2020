﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	public SpriteRenderer spriteRenderer => GetComponent<SpriteRenderer>();
	public Color color {
		get => spriteRenderer.color;
		set => spriteRenderer.color = value;
	}

	public Vector2 Velocity {
		get => GetComponent<Rigidbody2D>().velocity;
		set => GetComponent<Rigidbody2D>().velocity = value;
	}

	CircleCollider2D circleCol => GetComponent<CircleCollider2D>();
	public bool isFriendly = true;
	public float damage = 5;
	public float lifetime = 2.5f;
	private Vector2 lastPos;

	private void Start() {
		lastPos = transform.position;
	}

	private void Update() {

		lifetime -= Time.deltaTime;

		if(lifetime <= 0) {
			Destroy(gameObject);
		}

		CheckCollisions();
		lastPos = transform.position;
	}

	private void CheckCollisions() {

		Vector2 dif = (Vector2)transform.position - lastPos;
		RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, circleCol.radius, dif, dif.magnitude);

		foreach(RaycastHit2D hit in hits) {
			if(hit.collider != null) {
				hitCollider(hit.collider);
			}
		}
	}

	private void hitCollider(Collider2D collider) {

		Stats stats = collider.GetComponent<Stats>();

		if(stats != null) {
			if(isFriendly) {
				if(collider.GetComponent<Player>() != null)
					return;
			}
			else if(collider.GetComponent<Monster>() != null)
				return;
			stats.takeDamage(damage);
			Destroy(gameObject);
		}

		int solidLayer = LayerMask.NameToLayer("Solids");
		if(collider.gameObject.layer == solidLayer) {
			Destroy(gameObject);
		}
	}

	public static Projectile Fire(Projectile prefab, Vector2 position, float direction, float speed) {

		Projectile p = Instantiate(prefab);
		
		Vector2 vel = new Vector2(Mathf.Cos(direction) * speed, Mathf.Sin(direction) *  speed);
		p.transform.position = position;
		p.transform.rotation = Quaternion.Euler(0, 0, direction * Mathf.Rad2Deg);
		p.Velocity = vel;

		return p;
	}
}
