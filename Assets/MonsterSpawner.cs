﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
	public Monster toSpawn = default;
	public float spawnRadius = 10;
	public float spawnDelay = 3.5f;
	private float spawnWait = 0;

	private Vector2 findSpawnPos() {

		float randDir = Random.Range(0, 2) * Mathf.PI;
		Vector2 randRadius = new Vector2(Mathf.Cos(randDir), Mathf.Sin(randDir)) * spawnRadius;
		randRadius.y += 1.15f;

		return (Vector2)transform.position + randRadius;
	}

	private void spawnMonster() {

		Monster monster = Instantiate<Monster>(toSpawn);
		monster.transform.position = findSpawnPos();
	}

	void Update()
	{
		spawnWait -= Time.deltaTime;
		if(spawnWait <= 0) {
			spawnWait = spawnDelay;
			spawnMonster();
		}
	}
}
